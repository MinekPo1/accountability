# tech.lgbt Suspension

## Preamble and Disclaimer

This document is not a critique or attack on the tech.lgbt admins. While I disagree with their decision, I respect and understand it.

The following document contains mentions to topics which may be triggering or hurtful to some readers: Nazism and the Holocaust.  
In no way do I endorse these topics.

## Background

On 22-11-23 I saw and commented under a toot about veganism. In my reply I mentioned that I do not understand the moral argument used by supported of the idea.

This sent me into a self-reflection about my morality.

## Offending toots

> 2022-11-23T14:25:59Z
>
> CW: Morality (or lack there of)
>
> ***
>
> About my post about veganism saying "I don't understand
> the moral arguments pro or against veganism":
> 
> I have a very week moral compass, I won't get into it
> that much I have high confidence the moral compass itself
> is not that ethical.
> 
> Instead I use different factors, such as efficiency,
> common good etc. Things that are easier to mark
> rationally.
> 
> The outcome is usually close to what is considered moral,
> but the reasoning can be quite different. I its ok.

(note: the toot above was not cited as a reason for my suspension, but its part of the same thread)

***

> 2022-11-23T14:38:49Z
>
> CW: Morality (or lack there of), mention of Nazism and
> Holocaust
>
> ***
>
> I feel like if I was in such an environment, convincing
> me of extermination level racism wouldn't be hard. It
> doesn't take much for me to consider a living organism,
> no matter of intelligence a resource.
> 
> In fact one of the things that strikes me about the
> Holocaust is how many things you can do with a human, who
> you do not consider equal to your self.
> 
> Even the content warning seems fucked up gosh...

***

> 2022-11-23T14:40:20Z
>
> CW: [Clarification] Morality (or lack there of), mention
> of Nazism and Holocaust
>
> ***
>
>  In case it wasn't clear: many things better than what was
>  done.

***

> 2022-11-23T14:48:31Z
>
> CW: TL;DR of the toots containing morality (or lack there 
> of), mention of Nazism and the Holocaust (hopefully
> safe)
>
> ***
>
> My morality is shit, it doesn't take much to convince me 
> a being is but a resource, I think the III Reich could
> have done "better" (more useful to them) things with
> Jewish people.

## Explanation

I did not mean to say that I agree with the Nazis that Jews are lesser than humans,
rather that I do not understand them *even with* that assumption.

The entire thread was kinda a vent, and was not meant to show my ideology,
rather my morality.

As I said in the beginning of this document, I do not endorse Nazism,
nor do I believe any human is inherently worth less.
