# Accountability

Everyone makes mistakes and I am no different.

I believe that one's mistakes shouldn't be hidden in most cases, instead being being public.

This is my way of following this philosophy.

All documents here, except for showing my mistake, will add a explanation. Explanation, not excuse.  
I do not mean to critique, nor attack anyone in this repo.

## License

The documents here may not be used for commercial benefit.  
If quoted, a direct link must follow the quote.
